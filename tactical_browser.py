"""  _____           _   _           _     ___                                 
''' /__   \__ _  ___| |_(_) ___ __ _| |   / __\_ __ _____      _____  ___ _ __ 
'''   / /\/ _` |/ __| __| |/ __/ _` | |  /__\// '__/ _ \ \ /\ / / __|/ _ \ '__|
'''  / / | (_| | (__| |_| | (_| (_| | | / \/  \ | | (_) \ V  V /\__ \  __/ |   
'''  \/   \__,_|\___|\__|_|\___\__,_|_| \_____/_|  \___/ \_/\_/ |___/\___|_|   
'''                                                                           
'''                                   .: s0lst1c3 .: OGSystems OFFS3C Team
'''
'''                                           version 0.0.0
"""
import requests
import grey_harvest
import time
from   random import randrange

# module defaults
NUM_PROXIES          = 5
NUM_USER_AGENTS      = 4
USER_AGENT_FILE      = 'user_agents.txt'

DISCARD_OLD_SIGS     = False
DEFAULT_USER_AGENT   = '%s' %\
    'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'


class TacticalBrowser(requests.Session):

    def __init__(self,
            proxy_list=[],
            user_agents=[],
            discard_old_sigs=DISCARD_OLD_SIGS,
            default_user_agent=DEFAULT_USER_AGENT):

        requests.Session.__init__(self)

        self.proxy_list  = proxy_list
        self.user_agents = user_agents+[DEFAULT_USER_AGENT]

        self.discard_old_sigs = discard_old_sigs
        self.use_proxies = (proxy_list != [])

        self.anonymize()

    def clear_cookies(self):

        self.cookies.clear()

    def change_user_agent(self):

        next_user_agent = str(self._anonymize_helper(self.user_agents))
        self.headers['User-Agent'] = next_user_agent

    def change_proxy(self):

        next_proxy = str(self._anonymize_helper(self.proxy_list))
        self.proxies = {
            'https' : 'https://%s' % (next_proxy),
            'http' : 'http://%s' % (next_proxy),
        }

        
    def anonymize(self, sleep=-1):

        self.clear_cookies()
        self.change_user_agent()
        if self.use_proxies:
            self.change_proxy()
        
        if sleep >= 0:
            time.sleep(sleep)

    def _anonymize_helper(self, resource_list):

        if resource_list == []:
            raise IndexError('Signature lists depleted... repopulate.')
        index = randrange(0, len(resource_list))
        next_resource = resource_list[index]

        if self.discard_old_sigs:
            resource_list.pop(index)

        return next_resource

def spawn_proxy_list(length=NUM_PROXIES):

    harvester = grey_harvest.GreyHarvester()
    proxies = harvester.run()
    return [ proxies.next() for i in xrange(0, length) ]

def spawn_user_agent_list(length=NUM_USER_AGENTS, input_file=USER_AGENT_FILE):

    with open(input_file) as input_handle:
        lines = ( line.rstrip() for line in input_handle )
        return [ lines.next() for i in xrange(0, length) ]
