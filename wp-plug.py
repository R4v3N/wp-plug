#!/usr/bin/python
from bs4 import BeautifulSoup
import requests
import re
import argparse
import sys

class WP_Plug(object):
	
	def __init__(self, domain, enable_logs=False):
		self.domain = domain
		self.enable_logs = enable_logs

	def run(self):
		varurl = self.domain
		url = 'http://%s' % (varurl)
		response = requests.get(url)
		page = BeautifulSoup(response.text,'html.parser')
		for i in self._process_page(page):
			yield i

	def _get_wp_themes(self, page):
		wpsett = set()
		for link in page.findAll('link'):
			links = link.get('href')
			if 'wp-content/themes' in links:
				if re.match('(http:\/\/.*)',links):
					wpsett.add(links)
		return wpsett
	
	def _get_wp_plugins(self, page):
		wpsetp = set()
		for link in page.findAll('link'):
			links = link.get('href')
			if 'wp-content/plugins' in links:
				if re.match('(http:\/\/.*)',links):
						wpsetp.add(links)
		return wpsetp
	
	def _process_page(self, page):

		varurl = self.domain
		wpsetp = set()
		wpsett = set()
		urls = set()
		wpsetp = self._get_wp_plugins(page)
		wpsett = self._get_wp_themes(page)
		for link in page.findAll('a'):
			links = link.get('href')
			if links:
				if varurl in links:
					if re.match('(http:\/\/.*)',links):
							urls.add(links)
		for url in urls:
			try:
				response = requests.get(url, headers={'User-Agent' : 'Balls'})
				page = BeautifulSoup(response.text,'html.parser')
				if self.enable_logs:
					print 'url being processed:  %s' % (url)
				wpsetp.update(self._get_wp_plugins(page))
				wpsett.update(self._get_wp_themes(page))
			except urllib2.HTTPError as e:
				if self.enable_logs:
					print "exception fired %s" %(e.code)
		for i in wpsetp:
			yield i
		for i in wpsett:
			yield i

if __name__ =='__main__':
	
	print '''
                                 .__                
__  _  ________           ______ |  |  __ __  ____  
\ \/ \/ /\____ \   ______ \____ \|  | |  |  \/ ___\ 
 \     / |  |_> > /_____/ |  |_> >  |_|  |  / /_/  >
  \/\_/  |   __/          |   __/|____/____/\___  / 
         |__|             |__|             /_____/ 
					VERSION 0.0.2

	'''

	
	''' parse command line arguments '''
	parser = argparse.ArgumentParser()
	parser.add_argument('-d', '--domain',
                        help='./wp-plug.py -d example.com',)
	args = parser.parse_args()
	if not (args.domain):
		print '[*] USAGE: ./wp-plug.py -d mysite.com'
		sys.exit()
	domain = args.domain

	''' spawn a WP_Plug object '''
	wp_plug = WP_Plug(domain, enable_logs=True)

	print "*****the following plugins were found:*******"		
	for i in wp_plug.run():
		print "\t******%s" %(i)

